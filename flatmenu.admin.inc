<?php
/**
 * @file
 *   Admin configuration page.
 */

/**
 * Lists all flat menu items.
 */
function flatmenu_overview() {
  $output = '';
  $items = variable_get('flatmenu_items', array());
  if (empty($items)) {
    $output .= '<div>' . t('There are no flat menus.') . '</div>';
    $output .= l(t('Add a new flat menu'), 'admin/build/flatmenu/add');
  }
  else {
    $header = array(t('Name'), t('Menus'), t('Operations'));
    $rows = array();
    foreach ($items as $key => $item) {
      $operations = array();
      $operations[] = l(t('Edit'), "admin/build/flatmenu/edit/$key");
      $operations[] = l(t('Delete'), "admin/build/flatmenu/delete/$key");
      $rows[] = array(
        $item['itemname'],
        theme('item_list', $item['itemmenus']),
        theme('item_list', $operations)
      );
    }
    $output .= theme('table', $header, $rows);
  }
  return $output;
}

/**
 * Builds the add/edit form.
 *
 * @see flatmenu_form_submit().
 */
function flatmenu_form(&$form_state, $item = NULL) {
  $items = variable_get('flatmenu_items', array());
  $form = array();
  $form['itemname'] = array(
    '#type' => 'textfield',
    '#title' => t('Block description'),
    '#default_value' => isset($items[$item]) ? $items[$item]['itemname'] : '',
    '#required' => TRUE,
    '#maxlength' => 64,
    '#description' => t('A brief description of your block. Used on the <a href="@overview">block overview page</a>.', array('@overview' => url('admin/build/block'))),
  );
  $form['itemmenus'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Menus'),
    '#options' => menu_get_menus(TRUE),
    '#default_value' => isset($items[$item]) ? $items[$item]['itemmenus'] : array(),
    '#required' => TRUE,
  );
  $form['#item'] = $item;
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit handler for flatmenu_form().
 */
function flatmenu_form_submit($form, &$form_state) {
  $items = variable_get('flatmenu_items', array());
  $values = array(
    'itemname' => $form_state['values']['itemname'],
    'itemmenus' => array_filter($form_state['values']['itemmenus']),
  );
  // A new flatmenu item
  if (empty($form['#item'])) {
    $items['flatmenu-item-' . (count($items) + 1)] = $values;
  }
  else {
    $items[$form['#item']] = $values;
  }
  variable_set('flatmenu_items', $items);
  $form_state['redirect'] = 'admin/build/flatmenu';
}

/**
 * Confirmation form to delete a flatmenu item.
 *
 * @see flatmenu_delete_item_confirm_form_submit().
 */
function flatmenu_delete_item_confirm_form($form_state, $item = NULL) {
  $items = variable_get('flatmenu_items', array());
  if (!isset($items[$item])) {
    return MENU_NOT_FOUND;
  }
  $form['#itemid'] = $item;
  return confirm_form($form, t('Are you sure you want to delete the flat menu %title?', array('%title' => $items[$item]['itemname'])), 'admin/build/flatmenu', NULL, t('Delete'));
}

/**
 * Submit handler for flatmenu_delete_item_confirm_form().
 */
function flatmenu_delete_item_confirm_form_submit($form, &$form_state) {
  $items = variable_get('flatmenu_items', array());
  $name = $items[$form['#itemid']]['itemname'];
  unset($items[$form['#itemid']]);
  variable_set('flatmenu_items', $items);
  drupal_set_message(t('Deleted flat menu %name.', array('%name' => $name)));
  // Delete the block and clear the cache. Same as
  // block_box_delete_submit() it does.
  db_query("DELETE FROM {blocks} WHERE module = 'flatmenu' AND delta = '%s'", $form['#itemid']);
  cache_clear_all();
  $form_state['redirect'] = 'admin/build/flatmenu';
}

